﻿Entregas Computação Embarcada


| Aula                       | Handout                                                                                                                | Entrega Parcial | Entrega Final | Pesquisa |
|----------------------------|------------------------------------------------------------------------------------------------------------------------|-----------------|---------------|----------|
| 01 - Introdução            | Feito    |     |     |          |
| 02 - Mutirão C             | Feito com a Vitória    |     |     |          |
| 03 - IOs Digitais          | [Feito](https://gitlab.com/IsabellaRO/Computacao-Embarcada/tree/master/Aulas/03-IO-Digital/Handout%20aula%203%20IOs)    |     |     | Periféricos: Entregue em papel,  Introdução: [Feito](https://gitlab.com/IsabellaRO/Computacao-Embarcada/blob/master/Aulas/03-IO-Digital/03%20-%20Entrega%20Pesquisa%20introdução%20Embarcados.pdf)|
| 04 - Projeto A             |          |     | [Sim](https://gitlab.com/IsabellaRO/Projeto1-Embarcados) |          |
| 05 - PIO - In/Out          | [Feito](https://gitlab.com/IsabellaRO/Computacao-Embarcada/tree/master/Aulas/05-PIO/Handout)    |     |     | Entregue em papel |
| 06 - Projeto A             |          |     | [Sim](https://gitlab.com/IsabellaRO/Projeto1-Embarcados) |          |
| 07 - PIO - IRQ             | Feito    | [Sim](https://gitlab.com/IsabellaRO/Computacao-Embarcada/tree/master/Aulas/07-PIO-IRQ/Entrega%20Parcial) | [Sim](https://gitlab.com/IsabellaRO/Computacao-Embarcada/tree/master/Aulas/07-PIO-IRQ/Entrega%20Final) | [Sim](https://gitlab.com/IsabellaRO/Computacao-Embarcada/blob/master/Aulas/07-PIO-IRQ/Pesquisa%20Aula%207%20Embarcados%20(1).pdf) |
| 08 - Projeto Menos é Mais  |          |     | [Sim](https://gitlab.com/antoniosigrist/Computacao-Embarcada/tree/master/Projetos/B%20-%20Menos%20é%20mais/src) |          |
| 09 - TickTack              | Feito | Feito | [Sim](https://gitlab.com/IsabellaRO/Computacao-Embarcada/tree/master/Aulas/09-TickTack/SAME70-TickTack/SAME70-TC-RTC/src) | [Sim](https://gitlab.com/IsabellaRO/Computacao-Embarcada/blob/master/Aulas/09-TickTack/Pesquisa%20aula%209%20-%20Embarcados.pdf) |
| 10 - Avaliação             |          |     | [Sim](https://gitlab.com/IsabellaRO/Computacao-Embarcada/tree/master/Avaliacao) |          |
| 11 - RTOS                  | Feito |     | [Sim](https://gitlab.com/IsabellaRO/Computacao-Embarcada/tree/master/Aulas/11-RTOS/FREERTOS_SAM_EXAMPLE1/FREERTOS_SAM_EXAMPLE1) | [Sim](https://gitlab.com/IsabellaRO/Computacao-Embarcada/blob/master/Aulas/11-RTOS/Pesquisa%20aula%2011%20embarcados.pdf) |
| 12 - RTOS-UART             | Pendente |     |     |     |
| 13 - RTOS-ADC              | [Sim](https://gitlab.com/IsabellaRO/Computacao-Embarcada/tree/master/Aulas/13%20-%20RTOS%20-%20ADC/LCD-EXT2/SAME70-LCD-EXT2/src) |     |     |     |
| 14 - Projeto Conectividade |          |     | [Sim](https://gitlab.com/IsabellaRO/Computacao-Embarcada/tree/master/Projetos/D%20-%20Conectividade/HM10-EXT1) |     |
| 15 - Projeto Final         |          |     | [Sim](https://gitlab.com/vitoriahmc/ProjetoFinalEmbarcados) |     |

As entregas parciais/finais estão descritas no handout da aula.
