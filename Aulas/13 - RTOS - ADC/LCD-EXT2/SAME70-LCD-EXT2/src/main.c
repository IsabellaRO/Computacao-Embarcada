#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

struct ili9488_opt_t g_ili9488_display_opt;


/** Reference voltage for AFEC,in mv. */
#define VOLT_REF        (3300)

/** The maximal digital value */
#define MAX_DIGITAL     (4095UL)

#define POT_AFEC         AFEC0
#define POT_AFEC_ID      ID_AFEC0
#define POT_AFEC_CH      AFEC_CHANNEL_5 // Pin PB2
#define POT_AFEC_CH_IR   AFEC_INTERRUPT_EOC_5
#define POT_THRESHOLD    50

/* Canal do sensor de temperatura */
#define AFEC_CHANNEL_TEMP_SENSOR 11

//Var globais
/** The conversion data is done flag */
volatile bool is_conversion_done = false;
volatile bool is_conversion_done2 = false;

/** The conversion data value */
volatile uint32_t g_ul_value = 0;
volatile uint32_t g_ul_value2 = 0;


/**
}
/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	sysclk_enable_peripheral_clock(ID_PIOB);
	sysclk_enable_peripheral_clock(ID_PIOA);
	pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4);  // RX
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;

	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

//Afec adc config
void POT_init(void){
	pmc_enable_periph_clk(POT_AFEC_ID);
}

static int32_t convert_adc_to_temp(int32_t ADC_value){

  int32_t ul_vol;
  int32_t ul_temp;

	ul_vol = ADC_value * VOLT_REF/MAX_DIGITAL;

  /*
   * According to datasheet, The output voltage VT = 0.72V at 27C
   * and the temperature slope dVT/dT = 2.33 mV/C
   */
  ul_temp = ((ul_vol - 720) * 100)/(233 + 27);
  
  return(ul_temp);
}

static void AFEC_Temp_callback(void)
{
	g_ul_value = afec_channel_get_value(POT_AFEC, POT_AFEC_CH);
	is_conversion_done = true;
}

static void AFEC_Temp_callback2(void)
{
	g_ul_value2 = afec_channel_get_value(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
	is_conversion_done2 = true;
}

static void config_ADC_POT(void){
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);
  
	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);
  
	/* configura call back */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_5,	AFEC_Temp_callback, 1); 
   
	/*** Configuracao especfica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, POT_AFEC_CH, &afec_ch_cfg);
  
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	afec_channel_set_analog_offset(AFEC0, POT_AFEC_CH, 0x200);
	
	/* Selecina canal e inicializa converso */  
	afec_channel_enable(AFEC0, POT_AFEC_CH);
	afec_enable_interrupt(AFEC0, POT_AFEC_CH);

}

static void config_ADC_TEMP(void){
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);
  
	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);
  
	/* configura call back */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_11,	AFEC_Temp_callback2, 1); 
   
	/*** Configuracao especfica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, &afec_ch_cfg);
  
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	afec_channel_set_analog_offset(AFEC0,AFEC_CHANNEL_TEMP_SENSOR, 0x200);
	
	/* Selecina canal e inicializa converso */  
	afec_channel_enable(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
	afec_enable_interrupt(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);

}



/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */
int main(void)
{
	// array para escrita no LCD
	uint8_t stingLCD[256];
	
	//vars
	int temp, ul_temp, ul_resistencia;
	uint32_t ul_vol;
	
	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	POT_init();
	config_ADC_POT();

	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);

	/* Inicializa e configura o LCD */
	configure_lcd();

	/* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	sprintf(stingLCD, "Computacao Embarcada %d", 2018);
	ili9488_draw_string(10, 300, stingLCD);
	
	/* Output example information. */
	afec_start_software_conversion(AFEC0);


	while (1) {
		if(is_conversion_done == true) {
			char resistence[10];
			
			ul_vol = (g_ul_value*1000)/MAX_DIGITAL;
			ul_resistencia = (int) ul_vol;
			sprintf(resistence, "%d Ohms", ul_resistencia);
			ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
			ili9488_draw_filled_rectangle(10, 300,  ILI9488_LCD_WIDTH-1, 320);
					
			if(ul_resistencia < 333){
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
					ili9488_draw_filled_rectangle(10, 300,  ILI9488_LCD_WIDTH-1, 320);
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_GREEN));
					ili9488_draw_string(10, 300, resistence);
			} else if (ul_resistencia < 666) {
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
					ili9488_draw_filled_rectangle(10, 300,  ILI9488_LCD_WIDTH-1, 320);
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_YELLOW));
					ili9488_draw_string(10, 300, resistence);
			} else {
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
					ili9488_draw_filled_rectangle(10, 300,  ILI9488_LCD_WIDTH-1, 320);
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
					ili9488_draw_string(10, 300, resistence);
			}
			
			afec_start_software_conversion(AFEC0);
			is_conversion_done = false;
			delay_ms (2500);
			
			config_ADC_TEMP();
		}
		
		if(is_conversion_done2 == true){
			char temperature[10];
			ul_temp = convert_adc_to_temp(g_ul_value2);
			temp = (int) ul_temp;
			sprintf(temperature, "%d oC", temp);
			ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
			ili9488_draw_filled_rectangle(10, 300,  ILI9488_LCD_WIDTH-1, 320);
					
			if(temp < 16){
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
					ili9488_draw_filled_rectangle(10, 300,  ILI9488_LCD_WIDTH-1, 320);
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_GREEN));
					ili9488_draw_string(10, 330, temperature);
				} else if (temp < 26) {
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
					ili9488_draw_filled_rectangle(10, 300,  ILI9488_LCD_WIDTH-1, 320);
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_YELLOW));
					ili9488_draw_string(10, 330, temperature);
				} else {
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
					ili9488_draw_filled_rectangle(10, 300,  ILI9488_LCD_WIDTH-1, 320);
					ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
					ili9488_draw_string(10, 330, temperature);
			}
			
			
			afec_start_software_conversion(AFEC0);
			is_conversion_done2 = false;
			delay_ms (2500);
			config_ADC_POT();
		}
	}
	afec_start_software_conversion(AFEC0);
	delay_ms(2500);
	configure_lcd();
	return 0;
}

