#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"

/************************************************************************/
/* defines                                                              */
/************************************************************************/

// Led placa
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)
#define LED_PIO PIOC

// Bot�o placa
#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

// Bot�es externos
#define BUT1_PIO PIOA
#define BUT1_PIO_ID ID_PIOA
#define BUT1_PIO_PIN 19
#define BUT1_PIO_PIN_MASK (1 << BUT1_PIO_PIN)

#define BUT2_PIO PIOC
#define BUT2_PIO_ID ID_PIOC
#define BUT2_PIO_PIN 31
#define BUT2_PIO_PIN_MASK (1 << BUT2_PIO_PIN)


#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

struct ili9488_opt_t g_ili9488_display_opt;

/**
}
/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

void but_callBack(void);
void configura_botao(void);
void configura_led(void);

Bool but_flag = false;
Bool but1_flag = false;
Bool but2_flag = false;

void but_callBack(void){
	but_flag = true;
}

void but1_callBack(void){
	but1_flag = true;
}

void but2_callBack(void){
	but2_flag = true;
}

void pisca_led(int n){
	
	for(int i = 0; i < 5; i++){
		//clear, 0, acende
		pio_clear(LED_PIO, LED_PIO_PIN_MASK);
		delay_ms(n);
	
		//set, 1
		pio_set(PIOC, LED_PIO_PIN_MASK);
		delay_ms(n);
	}
	
	
	return ;
}

void configura_botao(void){
	pmc_enable_periph_clk(BUT_PIO_ID); //perif�rico clock
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_RISE_EDGE, but_callBack);
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 0);
}

void configura_botao1(void){
	pmc_enable_periph_clk(BUT1_PIO_ID); //perif�rico clock
	pio_configure(BUT1_PIO, PIO_INPUT, BUT1_PIO_PIN_MASK, PIO_PULLUP);
	pio_enable_interrupt(BUT1_PIO, BUT1_PIO_PIN_MASK);
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIO_PIN_MASK, PIO_IT_RISE_EDGE, but1_callBack);
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 1);
}

void configura_botao2(void){
	pmc_enable_periph_clk(BUT2_PIO_ID); //perif�rico clock
	pio_configure(BUT2_PIO, PIO_INPUT, BUT2_PIO_PIN_MASK, PIO_PULLUP);
	pio_enable_interrupt(BUT2_PIO, BUT2_PIO_PIN_MASK);
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIO_PIN_MASK, PIO_IT_RISE_EDGE, but2_callBack);
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 2);
}

void configura_led(void){
	//Ativa o perif�rico respons�vel pelo controle do LED
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_configure(PIOC, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_clear(PIOC, LED_PIO_PIN_MASK); //0
	pio_set(PIOC, LED_PIO_PIN_MASK); // 1
}

void atualiza_lcd(int n){
	/* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	
	float f = 1.0/(0.001*n); //Delay est� em ms
	uint8_t s[256];
	
	sprintf(s, "Computacao Embarcada %d", 2018);
	ili9488_draw_string(10, 300, s);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 320,  ILI9488_LCD_WIDTH-1, 340);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	sprintf(s, "Frequencia atual: %.1f Hz. :D", f);
	ili9488_draw_string(10, 320, s);
}


/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */
int main(void)
{
	// array para escrita no LCD
	uint8_t stingLCD[256];
	int n = 200;
	float f = 1.0/(0.001*n); //Delay est� em ms
	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);

    /* Inicializa e configura o LCD */
	configure_lcd();

    /* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	
	sprintf(stingLCD, "Computacao Embarcada %d", 2018);
	ili9488_draw_string(10, 300, stingLCD);
	sprintf(stingLCD, "Frequencia atual: %.1f Hz. :D", f);
	ili9488_draw_string(10, 320, stingLCD);

	//Desativa WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;

	configura_led();
	configura_botao();
	configura_botao1();
	configura_botao2();
	
	while (1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		if (but_flag){
			pisca_led(n);
			but_flag = false;
		}
		if (but1_flag){
			n += 50;
			atualiza_lcd(n);
			but1_flag = false;
		}
		if (but2_flag){
			n -= 50;
			atualiza_lcd(n);
			but2_flag = false;
		}
	}
	return 0;
}
