/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

// Led placa
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)
#define LED_PIO PIOC

// Bot�o placa
#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)



/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/

/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.

void but_callBack(void);
void configura_botao(void);
void configura_led(void);

Bool but_flag = false;

void but_callBack(void){
	but_flag = true;
}
	
void pisca_led(){
		
	//clear, 0, acende
	pio_clear(LED_PIO, LED_PIO_PIN_MASK);
	delay_ms(200);
	
	//set, 1
	pio_set(PIOC, LED_PIO_PIN_MASK);
	delay_ms(200);
	
	
	return ;
}

void configura_botao(void){
	pmc_enable_periph_clk(BUT_PIO_ID); //perif�rico clock
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_RISE_EDGE, but_callBack);
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 0);
}
void configura_led(void){
	//Ativa o perif�rico respons�vel pelo controle do LED
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_configure(PIOC, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_clear(PIOC, LED_PIO_PIN_MASK); //0
	pio_set(PIOC, LED_PIO_PIN_MASK); // 1
}

int main(void){
	
	//Initialize the board clock
	sysclk_init();
	
	//Desativa WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	configura_led();
	configura_botao();
	
	while (1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		if (but_flag){
			pisca_led();
			but_flag = false;
		}
	}
	return 0;
}
