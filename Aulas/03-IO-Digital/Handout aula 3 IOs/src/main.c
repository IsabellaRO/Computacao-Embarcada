/**
 * 5 semestre - Eng. da Computação - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

// Led
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)
#define LED_PIO PIOC

// Button
#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/

/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	//Initialize the board clock
	sysclk_init();
	
	//Desativa WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	//Ativa o controle do LED
	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	pio_set(LED_PIO, LED_PIO_PIN_MASK);
	pio_clear(LED_PIO, LED_PIO_PIN_MASK);
	
	while(1){

		int valor = pio_get(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK);
		
		if(valor == 0){
			for(int i = 0;i<5;i++){
				//Clear LED
				pio_clear(LED_PIO, LED_PIO_PIN_MASK);
				
				delay_ms(200);
				
				//Set LED
				pio_set(LED_PIO, LED_PIO_PIN_MASK);
				
				delay_ms(200);
				

			}
			
			} else{
			pio_set(PIOC, LED_PIO_PIN_MASK);
		}
		
	}

	return 0;
}
