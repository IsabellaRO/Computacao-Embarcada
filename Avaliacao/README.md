﻿# Avaliação 1
#### Computação Embarcada 2018.1


## 1 - PIO
o pino/PIO/bit utilizado para ligar e desligar o motor é o PIO A, PIN 6, ID 10.

![Embarcados_1](Embarcados_1.jpg)


## 2 - Especificação e Descrição
A solução proposta utiliza o RTC para indicar a hora atual no LCD do OLED e também faz parte do funcionamento do alarme. O alarme é configurado com o clicar do botão da placa ATMEL SAM E70. A cada clique, 1 minuto a mais é o tempo que demorará para o alarme tocar. Por exemplo, se agora o botão for pressionado duas vezes, o alarme tocará em 2 minutos. Assim, quando o RTC indicar o horário atual (combinação de hora e minuto) e ele for igual ao configurado, o LED da placa ATMEL SAM E70 piscará e o horário  continuará sendo indicado no LCD normalmente.
