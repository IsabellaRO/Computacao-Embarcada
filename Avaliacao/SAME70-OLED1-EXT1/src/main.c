/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>

// Led placa
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)
#define LED_PIO PIOC

// Bot�o placa
#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)


#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

#define YEAR        2018
#define MOUNTH      4
#define DAY         9
#define WEEK        15
#define HOUR        11
#define MINUTE      00
#define SECOND      0

volatile uint8_t alarme = 1;
volatile uint8_t flag_led0 = 0;
Bool but_flag = false;
uint32_t minuto;

void RTC_init(void);
void pisca_Led(Pio *pio, uint32_t mask);

static void Button1_Handler(uint32_t id, uint32_t mask)
{
	alarme += 1;
	but_flag = true;
	pisca_Led(LED_PIO, LED_PIO_PIN_MASK);
	pisca_Led(LED_PIO, LED_PIO_PIN_MASK);
}


void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);
	
			uint32_t hour1;
			uint32_t minute1;
			uint32_t second1;
			rtc_get_time(RTC, &hour1, &minute1, &second1);
		if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
	}
			
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);	
		
		
		flag_led0 = !flag_led0;
		
		if(flag_led0){
			pmc_enable_periph_clk(ID_TC1);	
			
			if(minute1 == minuto){
				alarme = 1;
				for(int i = 0; i < 10; i++){
					pisca_Led(LED_PIO, LED_PIO_PIN_MASK);
				}
				
				minuto = minute1+alarme;
				rtc_set_time_alarm(RTC, 1, hour1, 1, minuto, 1, second1);
			}
			flag_led0 = 0;
		}
		else{
			pmc_disable_periph_clk(ID_TC1);
			flag_led0 = 0;
		}
	}
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
}


void configura_led(int signal){
	//Ativa o perif�rico respons�vel pelo controle do LED
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_set_output(LED_PIO, LED_PIO_PIN_MASK, signal, 0, 0 );
}

void pisca_Led(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask)){
		pio_clear(pio, mask);}
	else{
		pio_set(pio,mask);
	}
}

void configura_botao(void){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_set_input(BUT_PIO, BUT_PIO_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
};

//Inicializando o RTC
void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN | RTC_IER_SECEN);

}

int main (void)
{
	char s[150];
	board_init();
	sysclk_init();
	delay_init();
	gfx_mono_ssd1306_init();
	
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	uint32_t hour1;
	uint32_t minute1;
	uint32_t second1;
	rtc_get_time(RTC, &hour1, &minute1, &second1);
	minuto = minute1 + alarme;
	rtc_set_time_alarm(RTC, 1, hour1, 1, minuto, 1, second1);
	gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
	sprintf(s, "%02d:%02d", hour1, minute1);
    gfx_mono_draw_string(s, 0, 0, &sysfont);
	
	/** Configura RTC */
	RTC_init();
	configura_led(1);
	configura_botao();
				
	while(1) {
		uint32_t hour1;
		uint32_t minute1;
		uint32_t second1;
		rtc_get_time(RTC, &hour1, &minute1, &second1);
		
		if(second1 == 59){
			minute1 ++;
			second1 = 0;
			sprintf(s, "%02d:%02d", hour1, minute1);
			gfx_mono_draw_string(s, 0, 0, &sysfont);
			}
			
		if(minute1 == 59 & hour1 == 23){
			hour1 = 0;
			minute1 = 0;
			sprintf(s, "%02d:%02d", hour1, minute1);
			gfx_mono_draw_string(s, 0, 0, &sysfont);
		}
		
		if(minute1 == 59){
			hour1 ++;
			minute1 = 0;
			sprintf(s, "%02d:%02d", hour1, minute1);
			gfx_mono_draw_string(s, 0, 0, &sysfont);
		}
	
		if (but_flag){
			alarme++;
			minuto = minute1 + alarme;
			/* configura alarme do RTC */
			rtc_set_time_alarm(RTC, 1, hour1, 1, minuto, 1, second1);
			but_flag = false;
		}
	}
}